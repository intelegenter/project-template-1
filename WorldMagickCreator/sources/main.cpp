#include "mainwindow.h"

#include <QApplication>
#include <QTranslator>
#include <QDebug>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  QTranslator RuTranslator;
  if(RuTranslator.load("translations/WorldMagickCreator_ru_RU.qm"))
    a.installTranslator(&RuTranslator);
  QTranslator DefaultRuTranslator;
  if(DefaultRuTranslator.load("translations/qt_ru.qm"))
    a.installTranslator(&DefaultRuTranslator);
  MainWindow w;
  w.show();
  return a.exec();
}
