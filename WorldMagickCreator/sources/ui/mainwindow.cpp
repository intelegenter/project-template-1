#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_compareButton_clicked()
{
  ui->resultLineEdit->setText(QString::number(ui->op1LineEdit->text().compare(ui->op2LineEdit->text())));
}
