QT += testlib
QT += core
QT += gui
QT += widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

SOURCES += \
  tst_mainwndtest.cpp

include(../../WorldMagickCreator/sources/ui/ui.pri)

INCLUDEPATH += $$PWD/../../WorldMagickCreator
