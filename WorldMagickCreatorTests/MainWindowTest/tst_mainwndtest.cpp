#include <QtTest>
#include <QCoreApplication>
#include <sources/ui/mainwindow.h>
#include <build/WorldMagickCreator/ui_mainwindow.h>
#include <QtTest/QtTest>

class MainWndTest : public MainWindow
{
  Q_OBJECT

public:
  MainWndTest();
  ~MainWndTest();

private slots:
  void initTestCase();
  void cleanupTestCase();
  void test_equal_strings();
  void test_a_is_substr_of_b();
  void test_b_is_substr_of_a();
  void test_same_lengh_last_sym_diff();
  void test_same_lengh_last_sym_diff_minus();
  void test_same_lengh_mid_sym_diff();
  void test_same_lengh_mid_two_sym_diff();
};

MainWndTest::MainWndTest()
{

}

MainWndTest::~MainWndTest()
{

}

void MainWndTest::initTestCase()
{
}

void MainWndTest::cleanupTestCase()
{
}

void MainWndTest::test_equal_strings()
{
  QTest::keyClicks(ui->op1LineEdit, "this is a string");
  QTest::keyClicks(ui->op2LineEdit, "this is a string");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(0));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_a_is_substr_of_b()
{
  QTest::keyClicks(ui->op1LineEdit, "abcde");
  QTest::keyClicks(ui->op2LineEdit, "abcdefghij");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(-1));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_b_is_substr_of_a()
{
  QTest::keyClicks(ui->op1LineEdit, "abcdefghij");
  QTest::keyClicks(ui->op2LineEdit, "abcde");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(1));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_same_lengh_last_sym_diff()
{
  QTest::keyClicks(ui->op1LineEdit, "abcdf");
  QTest::keyClicks(ui->op2LineEdit, "abcde");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(1));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_same_lengh_last_sym_diff_minus()
{
  QTest::keyClicks(ui->op1LineEdit, "abcde");
  QTest::keyClicks(ui->op2LineEdit, "abcdf");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(-1));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_same_lengh_mid_sym_diff()
{
  QTest::keyClicks(ui->op1LineEdit, "abcde");
  QTest::keyClicks(ui->op2LineEdit, "abide");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(-6));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

void MainWndTest::test_same_lengh_mid_two_sym_diff()
{
  QTest::keyClicks(ui->op1LineEdit, "abcde");
  QTest::keyClicks(ui->op2LineEdit, "abiabracadabranahoide");
  QTest::mouseClick(ui->compareButton, Qt::LeftButton);
  QCOMPARE(ui->resultLineEdit->text(), QString::number(-6));
  ui->op1LineEdit->clear();
  ui->op2LineEdit->clear();
}

QTEST_MAIN(MainWndTest)

#include "tst_mainwndtest.moc"
